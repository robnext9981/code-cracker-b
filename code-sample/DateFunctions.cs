﻿using System;

namespace code_cracker
{
    public static class DateFunctions
    {
        public static int GetTotalSecondsFromBirthDay(DateTime dayOfBirth){
            var secondsFromBirth = DateTime.Now.Subtract(dayOfBirth).TotalSeconds;
            return (int)secondsFromBirth;
        }
    }
}