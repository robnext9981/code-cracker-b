﻿using System;
using System.Globalization;

namespace code_cracker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce yourself - What's your name?");
            var name = Console.ReadLine();
            Console.WriteLine($"Hello {name}!");
            Console.WriteLine("Introduce yourself - What's your birthday? [dd/mm/yyyy]");
            var birthday = DateTime.ParseExact(Console.ReadLine(), "dd/mm/yyyy", CultureInfo.InvariantCulture);
            var secondsFromBirth = DateFunctions.GetTotalSecondsFromBirthDay(birthday);
            Console.WriteLine($"Phew! You're at least {secondsFromBirth} seconds old!");
        }
    }
}
